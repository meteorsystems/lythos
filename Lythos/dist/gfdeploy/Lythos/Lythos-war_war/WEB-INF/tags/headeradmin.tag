<%-- 
    Document   : headeradmin
    Created on : 30-mar-2013, 20:00:02
    Author     : IsmiKin
    Description : Cabecera para la vista de Administrador
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="brand" href="#">Proyecto Hermes</a>
            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active"> <a href="#"> Inicio </a> </li>
                    <li > <a href="#"> Lineas </a> </li>
                    <li > <a href="#"> Contabilidad </a> </li>
                    <li > <a href="#"> Listin Telefonico </a> </li>
                    <li > <a href="#"> Usuarios </a> </li>
                    <li > <a href="#"> Terminales </a> </li>
                    <li > <a href="#"> Gestion Interna </a> </li>
                </ul>
            </div>
        </div>
    </div>
</div>