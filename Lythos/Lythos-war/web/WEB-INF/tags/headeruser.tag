<%-- 
    Document   : headeruser
    Created on : 23-abr-2013, 13:00:57
    Author     : IsmiKin
    Description: cabecera para los usuarios normales (rol usuario)
--%>

<%@tag description="cabecera para los usuarios normales (rol usuario)" pageEncoding="UTF-8"%>

    <link rel="stylesheet" href="css/pages/home.css" />

    
    <!-- NAVBAR -->
    <div id="navbar" class="row-fluid">
            <div class="span12 column">
                    <div class="navbar">
                            <div class="navbar-inner  navbar-fixed-top">
                                    <div class="container-fluid">
                                             <a class="btn btn-navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a> <a href="#" class="brand">Proyecto Lythos</a>
                                            <div class="nav-collapse collapse navbar-responsive-collapse">
                                                    <ul class="nav menu-up">
                                                            <li class="menu-home">
                                                                    <a href="#">Home</a>
                                                            </li>
                                                            <li class="menu-perfil">
                                                                    <a href="#">Perfil</a>
                                                            </li>
                                                            <li class="menu-contabilidad">
                                                                    <a href="#">Contabilidad</a>
                                                            </li>
                                                            
                                                    </ul>
                                                    <ul class="nav pull-right">
                                                            <li>
                                                                    <a href="#">Ayuda</a>
                                                            </li>
                                                            <li class="divider-vertical">
                                                            </li>
                                                            <li class="dropdown">
                                                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Mi cuenta<strong class="caret"></strong></a>
                                                                    
                                                                    <ul class="dropdown-menu">
                                                                        <li class="nav-header">Mis cosas</li>
                                                                            <li>
                                                                                    <a href="#">Action</a>
                                                                            </li>
                                                                            <li>
                                                                                    <a href="#">Another action</a>
                                                                            </li>
                                                                            <li>
                                                                                    <a href="#">Something else here</a>
                                                                            </li>
                                                                            <li class="divider">                                                                                
                                                                            </li>
                                                                            <li class="nav-header">Mis otras cosas</li>
                                                                            <li>
                                                                                    <a href="#">Separated link</a>
                                                                            </li>
                                                                    </ul>
                                                            </li>
                                                    </ul>
                                            </div>

                                    </div>
                            </div>

                    </div>
            </div>

    </div>
    <!-- FIN DE NAVBAR -->
    
    <!-- BANNER -->
    
    
    
    <!-- FIN DE BANNER-->